<?php namespace itcom\yandexyml\Classes;

use Illuminate\Support\Facades\Log;
use itcom\yandexyml\Models\Settings;
use Lovata\Shopaholic\Models\Offer;
use October\Rain\Support\Facades\Flash;
use itcom\yandexyml\classes\ymlDocument;
use itcom\yandexyml\classes\ymlOffer;
use Lovata\Shopaholic\Models\Category;
use Lovata\Shopaholic\Models\Product;


/**
 * Class YandexYml
 *
 * @author a.tarasov
 * @package itcom\yandexyml\Classes
 */
class YandexYml
{
    /**
     * Путь к папке с файлом
     */
    const PATH_OUTPUT_FILE = '/shop.xml';

    /**
     * Путь к файлу валидации
     */
    const PATH_VALIDATE_FILE = 'itcom/yandexyml/validate/shops_with_byn.xsd';

    /**
     * @var ymlDocument $document
     */
    private $document;

    /**
     * @var
     */
    private $ymlFile;

    /**
     * Настройки
     */
    private $settings;

    /**
     * @return string
     */
    public function getYmlFolderPath()
    {
        return plugins_path(self::PATH_OUTPUT_FILE);
    }

    /**
     * @return string
     */
    public function getYmlFilePath()
    {
        if (!$this->ymlFile) {
            $this->ymlFile = base_path(self::PATH_OUTPUT_FILE);
        }

        return $this->ymlFile;
    }


    /**
     * Получить данные о расписании генерации файла
     *
     * @author a.tarasov
     * @access public
     * @return string
     */
    public static function getScheduleTime()
    {
        // <editor-fold defaultstate="collapsed" desc="code">

        return Settings::get('automatic_generate', 'never');

        // </editor-fold>
    }

    /**
     * @access private
     * @param $this ->settings
     * @return void
     */
    private function getMainDocument()
    {
        // Короткое название магазина, полное наименование компании, [кодировка, по умолчанию utf-8]
        $this->document = new ymlDocument($this->settings->market_name_short, $this->settings->market_name_full);

        // Имя файла
        $this->document->fileName(self::getYmlFilePath());

        // Адрес магазина. условно обязательный
        $this->document->url($this->settings->market_name_url);

        // CMS: название, [версия] они же 'platform' и 'version'
        $this->document->cms($this->settings->platform_name, $this->settings->platform_version);

        // Агенство, отвечающее за работоспособность сайта
        $this->document->agency($this->settings->site_agent_name);

        // Контактный адрес разработчиков CMS или агентства
        $this->document->email($this->settings->site_agent_contacts);

        // Валюта
        $this->document->currency($this->settings->market_currency, 1);

        // Категории
        if ($categories = Category::get()) {
            foreach ($categories as $category) {
                $this->document->category($category->id, $category->name,
                    $category->parent_id ? $category->parent_id : false);
            }
        }

        // Включение программы "Заказ на Маркете", можно еще передать false
        $this->document->cpa($this->settings->order_on_market ? true : false);
    }

    /**
     * @access private
     * @param Product $product
     * @return void
     */
    private function addProductToDocument($product)
    {
        if (!$this->document) {
            return false;
        }

        /**
         * @var Offer $productOffer
         */
        $productOffer = $product->offer->first();

        // Цена должна быть больше нуля
        if (empty($productOffer->price_value) || (int)ceil($productOffer->price_value) == 0) {
            return;
        }

        if (empty($product->name)) {
            return;
        }

        // Ограничения
        if (!$product->category_id) {
            return;
        }

        $offer = $this->document->simple($product->name, $product->id, (int)ceil($productOffer->price_value),
            $this->settings->market_currency, $product->category_id);

        // модель товара
        if ($product->property_value) {
            foreach ($product->property_value as $property) {
                if ($property->property->name == 'Модель') {
                    $offer->model($property->value->value);
                }
            }
        }

        // Производитель
        if (is_object($product->brand)) {
            $offer->vendor($product->brand->name);
        }
        // Код производителя для данного товара.
        $offer->vendorCode($product->code);
        // условно обязательный. URL страницы товара
        $offer->url($product->getFullUrl());
        // !!!  условно обязательные. Картинки
        if ($product->images) {
            foreach ($product->images as $image) {
                $offer->pic($image->getPath());
            }
        }
        // Описание с разрешенными тегами
        $offer->description($product->description, true);
    }

    /**
     * @access public
     * @return void
     */
    public function generate($manual = false)
    {
        // Получить настройки
        $this->settings = Settings::instance();

        // Сформировать основной документ
        $this->getMainDocument($this->settings);

        // Получить список товаров из каталога и добавить в YML документ
        if ($products = Product::get()) {
            foreach ($products as $product) {
                $this->addProductToDocument($product);
            }
        }

        // Сохранить документ
        $this->document->saveDocument();

        if ($manual) {
            $checker = new \DOMDocument('1.0', "UTF-8");
            $checker->load($this->getYmlFilePath());
            $valid = $checker->schemaValidate(plugins_path(self::PATH_VALIDATE_FILE));
            if ($valid) {
                Flash::success(trans('itcom.yandexyml::lang.yandexyml.messages.success.generate') . ". Файл сответствует схеме");
            }
            else {
                Flash::error("Файл не соответствует схеме");
            }
        }
    }
}