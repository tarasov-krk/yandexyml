<?php namespace itcom\yandexyml\Models;

use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'yandex_yml_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
