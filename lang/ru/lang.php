<?php return [
    'plugin'    => [
        'name'        => 'Яндекс YML',
        'description' => 'Генерация YML карты для яндекс маркета',
    ],
    'yandexyml' => [
        'settings'    => [
            'settings_name'                 => 'Яндекс YML',
            'settings_description'          => 'Настройки для генерации YML файла',
            'settings_generate_name'        => 'Генерация',
            'settings_generate_description' => 'Генерация YML файла',
            'settings_generate_hint'        => 'Ручной запуск генерации YML файла',
            'settings_generate_btn_start'   => 'Сгенерировать',

            'fields' => [
                'market_name_short'            => 'Краткое название магазина',
                'market_name_full'             => 'Полное название магазина',
                'market_name_url'              => 'Урл сайта',
                'file_path'                    => 'Урл сгенерированного файла',
                'order_on_market'              => 'Использовать заказ на яндекс маркете?',
                'platform_name'                => 'Название CMS',
                'platform_version'             => 'Версия CMS',
                'site_agent_name'              => 'Агенство, отвечающее за работоспособность сайта',
                'site_agent_contacts'          => 'Контактный адрес разработчиков CMS или агентства',
                'market_currency'              => 'Валюта магазина',
                'automatic_generate'           => 'Генерировать карту автоматически',
                'automatic_generate_on_1'      => 'Каждый час',
                'automatic_generate_on_3'      => 'Каждый 3 часа',
                'automatic_generate_on_6'      => 'Каждый 6 часов',
                'automatic_generate_on_12'     => 'Каждый 12 часов',
                'automatic_generate_on_24'     => 'Каждый день',
                'automatic_generate_on_month'  => 'Каждый месяц',
                'automatic_generate_on_weekly' => 'Каждую неделю',
                'automatic_generate_never'     => 'Никогда',
            ],
            'tabs'   => [
                'main'       => 'Основные',
                'additional' => 'Дополнительные',
                'currency'   => 'Валюта',
            ],
        ],
        'permissions' => [
            'manage_yandexyml' => 'Разрешить изменять настройки',
        ],
        'messages'    => [
            'success' => [
                'generate' => 'Успешная генерация YML файла',
            ],
        ],
    ],
];