<?php namespace itcom\yandexyml\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendMenu;
use itcom\yandexyml\Classes\YandexYml;

/**
 * Class Generate
 *
 * @package itcom\yandexyml\Controllers
 */
class Generate extends Controller
{

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('October.System', 'system', 'settings');
    }

    /**
     * @return void
     */
    public function index()
    {
        $this->pageTitle = 'itcom.yandexyml::lang.plugin.name';
    }

    /**
     * @return void
     */
    public function onGenerateYmlFile()
    {
        (new YandexYml)->generate(true);
    }

}