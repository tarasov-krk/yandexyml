<?php namespace itcom\YandexYml;

use itcom\yandexyml\Classes\YandexYml;
use System\Classes\PluginBase;
use Backend;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerPermissions()
    {
        return [
            'itcom.yandexyml.manage_yandexyml' => [
                'label' => 'itcom.yandexyml::lang.yandexyml.permissions.manage_yandexyml',
                'tab'   => 'itcom.yandexyml::lang.plugin.name',
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'itcom.yandexyml::lang.yandexyml.settings.settings_name',
                'description' => 'itcom.yandexyml::lang.yandexyml.settings.settings_description',
                'category'    => 'itcom.yandexyml::lang.plugin.name',
                'icon'        => 'oc-icon-yahoo',
                'order'       => 500,
                'class'       => 'itcom\yandexyml\Models\Settings',
                'keywords'    => 'Яндекс YML',
                'permissions' => ['itcom.yandexyml.manage_yandexyml'],
            ],
            'generate' => [
                'label'       => 'itcom.yandexyml::lang.yandexyml.settings.settings_generate_name',
                'description' => 'itcom.yandexyml::lang.yandexyml.settings.settings_generate_description',
                'category'    => 'itcom.yandexyml::lang.plugin.name',
                'icon'        => 'icon-magnet',
                'url'         => Backend::url('itcom/yandexyml/generate'),
                'order'       => 510,
                'keywords'    => 'Яндекс YML',
                'permissions' => ['itcom.yandexyml.manage_yandexyml'],
            ],
        ];
    }

    public function registerSchedule($schedule)
    {
        if ($scheduleTime = YandexYml::getScheduleTime() AND $scheduleTime != 'never') {
            $schedule->call(function () {

                \Log::error('Генерация UML файла');
                (new YandexYml)->generate();

            })->{$scheduleTime}();
        }
    }
}
